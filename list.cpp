#include<iostream>
#include<cstdlib>

#include"list.hpp"

namespace animalfarm{
   Node* head = new Node();

   const bool SingleLinkedList::empty() const{
      return head == NULL;
   }

   void SingleLinkedList::push_front( Node* newNode ){
      newNode->next = head;
      head = newNode;
   }

   Node* SingleLinkedList::pop_front(){
      if(head==NULL){
         return nullptr;
      }
      else{
         Node* temp = head;
         head = head->next;
         return temp;
   }
   }

   Node* SingleLinkedList::get_first() const{
      return head;
   }

   Node* SingleLinkedList::get_next( const Node* currentNode) const{
      return currentNode->next;
   }

   unsigned int SingleLinkedList::size() const{
      unsigned int i;
      Node* ind = head;
      for(i=0; ind != NULL; i++){
         ind = ind->next;
      }
      return i;
   }
}

