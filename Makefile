###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 05a - Animal Farm 2
#
# @file    Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

all: main 

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

fish.o: fish.hpp fish.cpp animal.hpp
	g++ -c fish.cpp

bird.o: bird.hpp bird.cpp animal.hpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp mammal.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp fish.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp fish.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp bird.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp bird.hpp
	g++ -c nene.cpp

animalfactory.o: animalfactory.cpp animalfactory.hpp
	g++ -c animalfactory.cpp

node.o: node.cpp node.hpp
	g++ -c node.cpp

list.o: list.cpp list.hpp
	g++ -c list.cpp

main: main.cpp *.hpp list.o node.o main.o animalfactory.o animal.o mammal.o cat.o dog.o nunu.o fish.o aku.o bird.o palila.o nene.o
	g++ -o main main.o list.o node.o animalfactory.o animal.o mammal.o cat.o dog.o nunu.o fish.o aku.o bird.o palila.o nene.o

clean:
	rm -f *.o main
