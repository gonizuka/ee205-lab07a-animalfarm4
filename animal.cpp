///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Gavin Onizuka <gonizuka@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   23_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <stdlib.h>
#include "animal.hpp"

using namespace std;

namespace animalfarm {

   Animal::Animal(){
      cout << ".";
   }

   Animal::~Animal(){
      cout << "x";
   }
	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}
const enum Gender Animal::getRandomGender(){
   int i = rand() % 3;

   switch(i){
      case 0: return MALE; break;
      case 1: return FEMALE; break;
      case 2: return UNKNOWN; break;
   }
   return MALE;
}

string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	
const enum Color  Animal::getRandomColor(){
   int i = rand() % 6;

   switch(i){
      case 0: return BLACK; break;
      case 1: return WHITE; break;
      case 2: return RED; break;
      case 3: return SILVER; break;
      case 4: return YELLOW; break;
      case 5: return BROWN; break;
   }
   return BLACK;
}

string Animal::colorName (enum Color color) {
	/// @todo Implement this based on genderName and your work
	///       on Animal Farm 1
   switch (color) {
      case BLACK:   return string("Black"); break;
      case WHITE:   return string("White"); break;
      case RED:     return string("Red"); break;
      case SILVER:  return string("Silver"); break;
      case YELLOW:  return string("Yellow"); break;
      case BROWN:   return string("Brown"); break;
   }
   return string("Unknown");
};

const bool Animal::getRandomBool(){
   int i = rand() % 2;
   switch(i){
      case 0: return false; break;
   }
   return true;
}

const float Animal::getRandomWeight(const float from, const float to){
   int i = (rand() % int(to - from));
   float weight = i + from;
   return weight;
}

const string Animal::getRandomName(){
   int len = (rand() % 6) + 4;
   string name;
   int up = (rand() % 26) + 65;
   name += char(up);

   for(int i=1; i < len; i++){
      int low = (rand() % 26) + 97;
      name += char(low);
   }

   return name;
}

} // namespace animalfarm
