#pragma once
#include "animal.hpp"

using namespace std;
namespace animalfarm{

   class AnimalFactory{
      public:
         static Animal* getRandomAnimal();
   };
}
